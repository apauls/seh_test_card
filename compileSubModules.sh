echo "Compile power_supply package ..."
echo " - - - - - - - - - - - - - - - -"

mkdir power_supply/build/
cd power_supply/build/
cmake3 ..
make -j$(nproc)

echo "            DONE               "
echo "                               "

echo "Compile cmsph2_tcusb package ..."
echo " - - - - - - - - - - - - - "

cd ../../
mkdir cmsph2_tcusb/build/
cd cmsph2_tcusb/
source ./setup.sh
cd build/
cmake3 ..
make -j$(nproc)

echo "            DONE               "
echo "                               "

echo "Compile Ph2_ACF package ..."
echo " - - - - - - - - - - - - - "

cd ../../
mkdir Ph2_ACF/build/
cd Ph2_ACF/
source ./setup.sh
export CompileWithTCUSB=true
cd build/
cmake3 ..
make -j$(nproc)

echo "            DONE               "
echo "                               "