cd /home/DAQUser/testcardDev/Ph2_ACF/
source setup.sh
fpgaconfig -c settings/2S_SEH.xml -b 1 -i 2s_seh_electrical_08062023.bin
fpgaconfig -c settings/2S_SEH.xml -b 0 -i 2s_12m_l12octa_fec5_latest.bin

cd /home/DAQUser/testcardDev/power_supply
source setup.sh

PowerSupplyController -c config/configRohdeSchwartz.xml

