<details>
  <summary>Installation and configuration instructions (for experienced operator)</summary>
  
- [ ] Clone this repository 
```
git clone --recurse-submodules https://gitlab.cern.ch/aachen1b/seh_test_card.git
```
- [ ] Run compileSubModules.sh
```
cd seh_test_card
bash compileSubModules.sh 
```
- [ ] Adjust the settings files in Ph2_ACF and power_supply folder 
    - FC7 IP adresses
    - lpgbt version
    - Power supply settings and channels
    - Upload appropiate firmware images

    <details>
    <summary>AMC13 setup</summary>

    In seh_test_card

    Settingsfile for Aachen AMC13 

    Loop back fiber in TTC 

    ```
    cp connectionSN318.xml amc13/amc13/etc/amc13/connectionSN318.xml
    cd amc13
    make UHAL_VER_MINOR=7
    source env.sh
    AMC13Tool2.exe -c amc13/etc/amc13/connectionSN318.xml
    en 1-12 t
    q
    cd ..
    ```

    </details>

</details>

# Manual for Test Card Testing 

## Prepare setup for testing (only once per session)

At the Linux PC (DAQUser)

- [ ] Check if power supplies are switched on
- [ ] Check if low voltage power supply channels are on
- [ ] Open fresh terminal 
- [ ] Load firmware (preseries tested with https://udtc-ot-firmware.web.cern.ch/?dir=v2-01 images)
- [ ] Start power_supply server
```
cd /home/DAQUser/testcardDev/Ph2_ACF/
source setup.sh
fpgaconfig -c settings/2S_SEH.xml -b 1 -i 2s_seh_electrical_08062023.bin
fpgaconfig -c settings/2S_SEH.xml -b 0 -i 2s_12m_l12octa_fec5_latest.bin

cd /home/DAQUser/testcardDev/power_supply
source setup.sh

PowerSupplyController -c config/configRohdeSchwartz.xml
```
or use setup.sh bash script 
- [ ] Confirm that power_supply server is running: 

> int TCPServerBase::accept(bool)Now server accept connections on socket: 3

- [ ] Leave terminal open while testing
- [ ] Open a fresh working terminal for the testing

<details>
<summary>Outdated</summary>
<summary>Ignore Instructions for Testing Test Cards</summary>

## Preparation of test card and initial scan 

At the Linux PC (DAQUser)

- [ ] Connect SEH "2SSEH-201000045" to test card as instructed
- [ ] Plug test card as instructed
- [ ] In the working terminal go to software folder and scan for the test card

```
cd /home/DAQUser/Felix/Ph2_ACF
source setup.sh
mux_setup -f settings/2S_SEH.xml --mux_scan 
```
A scan is needed after every firmware reload.

## Switch on test card

At the Linux PC (DAQUser)

- [ ] Turn on fan for cooling 
- [ ] In the working terminal go to software folder and configure test card

```
mux_setup -f settings/2S_SEH.xml --mux_configure 0,0
```

## WIP: Fuse ID and test with LabVIEW

At Windows PC (PilotDCDC)

- [ ] Make sure USB cable is connected to Windows PC
- [-] Fuse next "Product String" as instructed 
- [-] Label card with sticker
- [-] Add ID to excel table
- [ ] Run LabVIEW test 
    - Change ID variables where necessary
        - At bottom right data path is given (C:/users/.../Testcardtests/Card00xxx_y)
        - xxx should be the testcard ID, y is the iterative number of tests
        - Start with 1 and increase it if you need to run a second test
- [ ] Start test (arrow on top left) and wait until it's done (approx. 5 min)
- [ ] Connect USB cable to Linux PC 
</details>

## Run main test

At the Linux PC (DAQUser)

- [ ] In the working terminal:
```
cd /home/DAQUser/testcardDev/Ph2_ACF
source setup.sh
```
- [ ] Run test script 
    - Adjust test card number behind "-t" keyword
    - Adjust hybrid card number behind "-h" keyword

```
bash StartTest.sh -s 0 -o mlipinski -t CMSPH2-BRD00783 -h 2SSEH-213000xxx
```
> -o for operator. Type in your name (e.g., mlipinski)

> -t for the testcard ID (e.g., CMSPH2-BRD00594)

> -h for the hybrid ID (e.g., 2SSEH-213000xxx)


- [ ] At the end, enter a comment
- [ ] If the test was successful, the result folder is in "/home/DAQUser/testcardDev/Data"
- [ ] Copy successful tests (complete folder) from "/home/DAQUser/testcardDev/Data" result to "/home/DAQUser/v4_SEH_results/Data"

## Upload test and check results 

At Windows PC (PilotDCDC)

- [ ] In remote desktop session to data base PC (sehdb@134.61.4.137)
- [ ] In console go to software folder
```
cd C:\SEHTest\AnalysisScripts\SEHTestcard\
```
- [ ] Run analysis script with test result folder name as argument 
- [ ] Check test results in data base 
   http://134.61.4.137:3000/d/tBjpbACGz/main-dashboard?orgId=1&var-seh_series=Preseries&var-seh_delivery_status=1 

```
.\SEHTestcard.exe SEH_2SSEH-213000001_2024-09-03_16-02-27
```

